const mongoose = require("mongoose");

const { MONGO_URI } = process.env;

exports.connect = () => {
  mongoose
    .connect(MONGO_URI)
    .then(() => {
      console.log("Database connected successfully");
    })
    .catch((err) => {
      console.log(`Database failed to connect: ${err.message}. Exiting ...`);
      console.error(err);
      process.exit(1);
    });
};
