# epam-node-hw03

UBER like service for freight trucks, in REST style, using MongoDB as database.
This service should help regular people to deliver their stuff and help drivers to find loads
and earn some money. Application contains 2 roles, driver and shipper.

# Pre-requisites

- Install [Node.js](https://nodejs.org/en/)

# Getting started

- Clone the repository

```
git clone  <git lab template url> <project_name>
```

- Install dependencies

```
cd node-hw3
npm install
```

- Build and run the project

```
npm start
```

Navigate to `http://localhost:8080`

- API Document endpoints

  swagger Endpoint : http://localhost:8080/api/auth/login

  UI Endpoint : http://localhost:8080/login

## Project Structure

The folder structure of this app is explained below:

| Name                | Description                                                                                      |
| ------------------- | ------------------------------------------------------------------------------------------------ | ------------- | -------------------------------------------------------------------- |
| **config**          | Application configuration including environment-specific configs                                 |
| **src/api**         | Define functions to serve various express routes.                                                |
| **src/controllers** | Controllers define functions to serve various express routes.                                    |
| **src/middleware**  | Express middleware which process the incoming requests before handling them down to the routes   |
| **src/models**      | Models define schemas that will be used in storing and retrieving data from Application database |
| **src/routes**      | Contain all express routes, separated by module/area of application                              |
| **src/views**       | Contain render views for application                                                             |
| package.json        | Contains npm dependencies as well as [build scripts](#what-if-a-library-isnt-on-definitelytyped) | tsconfig.json | Config settings for compiling source code only written in TypeScript |
| .eslintrc.js        | Config settings for ESLint code style checking                                                   |

## Building the project

### Running the build

All the different build steps are orchestrated via [npm scripts](https://docs.npmjs.com/misc/scripts).
Npm scripts basically allow us to call (and chain) terminal commands via npm.

| Npm Script | Description                                                                     |
| ---------- | ------------------------------------------------------------------------------- |
| `start`    | Runs full build and runs node on dist/index.js. Can be invoked with `npm start` |
| `dev`      | Runs full build before starting all watch tasks. Can be invoked with `npm dev`  |
| `test`     | Runs build and run tests using mocha                                            |

# Swagger

The swagger specification file is named as swagger.yaml. The file is located under root folder.
Example:

```
paths:
  /api/users/me:
    get:
      tags:
      - "User"
      summary: "Get user's profile info"
      description: "User can request only his own profile info"
      operationId: "getProfileInfo"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        "200":
          description: "Success"
          schema:
            type: object
            properties:
              user:
                $ref: '#/definitions/User'
        "400":
          description: "Bad request"
          schema:
            $ref: '#/definitions/Error'
        "500":
          description: "Internal server error"
          schema:
            $ref: '#/definitions/Error'
      security:
      - jwt_token: []
    delete:
      tags:
      - "User"
      summary: "Delete user's profile"
      description: "User can delete only his own profile info"
      operationId: "deleteProfile"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        "200":
            description: "Success"
            schema:
                type: "object"
                properties:
                  message:
                    type: string
                    example: "Profile deleted successfully"
        "400":
          description: "Bad request"
          schema:
            $ref: '#/definitions/Error'
        "500":
          description: "Internal server error"
          schema:
            $ref: '#/definitions/Error'
      security:
      - jwt_token: []
```

# Views

The project views are available in src/views folder

If user's token is not provided or is deleted, the project will return JSON object

```
if (!token) {
    console.log("A token must be provided");
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
```
