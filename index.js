require("dotenv").config();
require("./config/database").connect();

const cookieParser = require("cookie-parser");
const cookieSession = require("cookie-session");
const express = require("express");
const port = process.env.SERVER_PORT || 8080;
require("./config/database");

const app = express();

// Views
app.set("views", ["./src/views/containers"]);
app.set("view engine", "pug");

// Parsing application/json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Cookies
app.use(cookieParser());
app.use(
  cookieSession({
    name: "session",
    keys: ["aBigSecret"],
    maxAge: 100 * 60 * 60 * 1000,
  })
);

// Routes
app.use(require("./src/routes/auth.route"));
app.use(require("./src/routes/user.route"));
app.use(require("./src/routes/truck.route"));
app.use(require("./src/routes/load.route"));

app.listen(port, () => {
  console.log(`Server running → PORT ${port}`);
});
