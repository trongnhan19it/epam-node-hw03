const jwt = require("jsonwebtoken");

const verifyToken = async (req, res, next) => {
  const token = req.headers?.authorization || req.cookies?.jwt_token;
  if (!token) {
    console.log("A token must be provided");
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
  // Decoded Token
  try {
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    req.user = decoded;
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
  return next();
};

module.exports = verifyToken;
