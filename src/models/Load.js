const mongoose = require("mongoose");

const dimensionSchema = mongoose.Schema({
  width: {
    type: Number,
    default: 0,
  },
  length: {
    type: Number,
    default: 0,
  },
  height: {
    type: Number,
    default: 0,
  },
});

const loadSchema = mongoose.Schema({
  name: {
    type: String,
    default: `New Load ${this._id}`,
  },
  created_by: {
    type: String,
    required: [true, "Original User is required"],
  },
  assigned_to: {
    type: String,
    default: "n/a",
  },
  status: {
    type: String,
    default: "NEW",
  },
  state: {
    type: String,
    default: "n/a",
    enum: ["n/a", "En route to Pick Up", "En route to Delivery", "Completed"],
  },
  payload: {
    type: Number,
    default: 0,
  },
  pickup_address: {
    type: String,
    required: [true, "Pickup Address is required"],
  },
  delivery_address: {
    type: String,
    required: [true, "Delivery Address is required"],
  },
  dimensions: {
    type: dimensionSchema,
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
      },
    },
  ],
  created_date: {
    type: Date,
    default: new Date(),
  },
});

const Load = mongoose.model("Load", loadSchema);

module.exports = Load;
