const mongoose = require("mongoose");

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: [true, "Original User is required"],
  },
  assigned_to: {
    type: String,
    default: "n/a",
  },
  type: {
    type: String,
    required: [true, "Truck type is required"],
  },
  status: {
    type: String,
    default: "IS",
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
});

const Truck = mongoose.model("Truck", truckSchema);

module.exports = Truck;
