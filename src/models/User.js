const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

/* ------------------------------- MODELS ------------------------------ */
const UserSchema = mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    minLength: 8,
    required: [true, "Password is required"],
  },
  role: {
    type: String,
    enum: ["DRIVER", "SHIPPER"],
    required: [true, "Role is required"],
  },
  status: {
    type: String,
    default: "occupied",
    enum: ["available", "occupied"],
  },
  tokens: [
    {
      token: {
        type: String,
        required: [true, "Token is required"],
      },
    },
  ],
  created_date: {
    type: Date,
    default: new Date(),
  },
});

/* ------------------------------ METHODS ------------------------------ */
// Encrypt password
UserSchema.pre("save", async function (next) {
  const user = this;
  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

// Token
UserSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign(
    {
      _id: user._id,
      email: user.email,
      password: user.password,
      role: user.role,
      created_date: user.created_date,
    },
    process.env.JWT_KEY
  );
  user.tokens[0] = { token };
  await user.save();
  return token;
};

// Find User in database
UserSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });
  const userIsValid = user && (await bcrypt.compare(password, user.password));
  if (!userIsValid) {
    return false;
  }
  return user;
};

const User = mongoose.model("User", UserSchema);

module.exports = User;
