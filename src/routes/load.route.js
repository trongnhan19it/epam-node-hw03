const express = require("express");
const auth = require("../middleware/auth");
const api = require("../api/load.api");
const view = require("../controllers/load.controller");

const router = express.Router();

//* ---------------------------------- VIEWS --------------------------------- */
router.get("/user/loads", auth, view.getLoad);
router.get("/user/loads/active", auth, view.getActiveLoad);
router.get("/user/loads/:id", auth, view.getLoadById);
router.post("/user/loads", auth, view.addLoad);
router.post("/user/loads/update/:id", auth, view.updateLoadById);
router.post("/user/loads/post/:id", auth, view.postLoadById);
router.post("/user/loads/delete/:id", auth, view.deleteLoadById);

//* -------------------------------- API ------------------------------- */
router.get("/api/loads", auth, api.getLoad);
router.post("/api/loads", auth, api.addLoad);
router.get("/api/loads/active", auth, api.getActiveLoad);
router.patch("/api/loads/active/state", auth, api.setActiveLoad);
router.get("/api/loads/:id", auth, api.getLoadById);
router.put("/api/loads/:id", auth, api.updateLoadById);
router.delete("/api/loads/:id", auth, api.deleteLoadById);
router.post("/api/loads/:id/post", auth, api.postLoadById);
router.get("/api/loads/:id/shipping_info", auth, api.getShippingInfo);

module.exports = router;
