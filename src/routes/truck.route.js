const express = require("express");
const auth = require("../middleware/auth");
const api = require("../api/truck.api");
const view = require("../controllers/truck.controller.js");

const router = express.Router();

//* ---------------------------------- VIEWS --------------------------------- */
router.get("/user/trucks", auth, view.getTruck);
router.post("/user/trucks", auth, view.addTruck);
router.post("/user/trucks/update/:id", auth, view.updateTruckById);
router.post("/user/trucks/assign/:id", auth, view.assignTruckById);
router.post("/user/trucks/delete/:id", auth, view.deleteTruckById);

//* -------------------------------- API ------------------------------- */
router.get("/api/trucks", auth, api.getTruck);
router.post("/api/trucks", auth, api.addTruck);
router.get("/api/trucks/:id", auth, api.getTruckById);
router.put("/api/trucks/:id", auth, api.updateTruckById);
router.delete("/api/trucks/:id", auth, api.deleteTruckById);
router.post("/api/trucks/:id/assign", auth, api.assignTruckById);

module.exports = router;
