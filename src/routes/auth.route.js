const express = require("express");
const view = require("../controllers/auth.controller");
const api = require("../api/auth.api");

const router = express.Router();

//* ---------------------------------- VIEWS ---------------------------------- */
router.get("/", (req, res) => {
  res.redirect("/login");
});
router.get("/login", (req, res) => {
  res.render("auth", { type: "LOGIN" });
});
router.get("/register", (req, res) => {
  res.render("auth", { type: "REGISTER" });
});
router.get("/forgot_password", (req, res) => {
  res.render("auth", { type: "FORGOT_PASSWORD" });
});
router.post("/login", view.login);
router.post("/register", view.register);
router.post("/forgot_password", view.forgotPassword);
router.post("/logout", view.logout);

//* ---------------------------------- API ---------------------------------- */
router.post("/api/auth/register", api.register);
router.post("/api/auth/login", api.login);
router.post("/api/auth/forgot_password", api.forgotPassword);

module.exports = router;
