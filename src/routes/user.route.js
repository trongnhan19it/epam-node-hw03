const express = require("express");
const auth = require("../middleware/auth");
const api = require("../api/user.api");
const view = require("../controllers/user.controller");

const router = express.Router();

//* ---------------------------------- VIEWS --------------------------------- */
router.get("/user/profile", auth, view.getUser);
router.post("/user/profile/delete", view.deleteUser);
router.post("/user/profile/password/change", auth, view.changePassword);

//* ---------------------------------- API --------------------------------- */
router.get("/api/users/me", auth, api.getUser);
router.delete("/api/users/me", auth, api.deleteUser);
router.patch("/api/users/me/password", auth, api.changePassword);

module.exports = router;
