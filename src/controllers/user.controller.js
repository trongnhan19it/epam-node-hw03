const bcrypt = require("bcryptjs");
const User = require("../models/User");
const Truck = require("../models/Truck");
const Load = require("../models/Load");

//* GET USER'S PROFILE INFO
const getUser = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Success
    return res.render("dashboard", { type: "PROFILE", user: user });
  } catch (err) {
    return res.redirect("/user/profile");
  }
};

//* DELETE USER'S PROFILE INFO
const deleteUser = async (req, res) => {
  try {
    User.findByIdAndDelete(req.body._id, async (err) => {
      if (err)
        return res.render("bad_request", {
          error: process.env.ERROR_MESSAGE_401,
        });
      const user = JSON.parse(req.body.user);

      const passwordIsIncorrect = !(await bcrypt.compare(
        req.body.password,
        user.password
      ));

      if (passwordIsIncorrect)
        return res.render("dashboard", {
          type: "PROFILE",
          user: user,
          error: "Password is incorrect",
        });

      // Delete User Profile
      const filter = { created_by: user._id };
      if (user.role === "DRIVER") await Truck.deleteMany({ where: filter });
      else await Load.deleteMany({ where: filter });

      // Success
      return res.redirect("/login");
    });
  } catch (err) {
    return res.redirect("/user/profile");
  }
};

//* CHANGE USER'S PASSWORD
const changePassword = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Check password
    const passwordIsIncorrect = !(await bcrypt.compare(
      req.body.oldPassword,
      user.password
    ));

    if (passwordIsIncorrect)
      return res.render("dashboard", {
        type: "PROFILE",
        user: user,
        error: "Password is incorrect",
      });

    // Change password
    const update = await bcrypt.hash(req.body.newPassword, 8);
    await User.updateOne({ where: { _id: user._id } }, { password: update });

    // Token
    const token = await user.generateAuthToken();
    res.setHeader("Cache-Control", "private");
    res.cookie("jwt_token", token, { sameSite: "lax" });

    // Success
    return res.render("dashboard", {
      type: "PROFILE",
      user: user,
      success: "Password successfully updated",
    });
  } catch (err) {
    return res.redirect("/user/profile");
  }
};

module.exports = { getUser, deleteUser, changePassword };
