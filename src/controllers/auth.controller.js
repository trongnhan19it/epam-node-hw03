const User = require("../models/User");

//* REGISTER A NEW SYSTEM USER
const register = async (req, res) => {
  try {
    // Create new User
    const newUser = new User(req.body);
    await newUser.save();
    const token = await newUser.generateAuthToken();

    // Success
    return res.render("auth", {
      type: "LOGIN",
      success: "User created",
      token,
    });
  } catch (err) {
    return res.render("auth", {
      type: "REGISTER",
      error: "Email already exists",
      err,
    });
  }
};

//* LOGIN
const login = async (req, res) => {
  try {
    // User authorization
    const { email, password } = req.body;
    const user = await User.findByCredentials(email, password);
    if (!user)
      return res.render("auth", {
        type: "LOGIN",
        error: "Email or password is incorrect",
      });

    // Generate new login token
    const token = await user.generateAuthToken();
    res.setHeader("Cache-Control", "private");
    res.cookie("jwt_token", token, { sameSite: "lax" });

    // Success
    return res.redirect("/user/profile");
  } catch (err) {
    return res.render("auth", {
      type: "LOGIN",
      error: process.env.ERROR_MESSAGE_500,
      err,
    });
  }
};

//* FORGOT PASSWORD
const forgotPassword = async (req, res) => {
  try {
    // User authorization
    const user = await User.findOne(req.body);
    if (!user)
      return res.render("auth", {
        type: "FORGOT_PASSWORD",
        error: "Email address not found",
      });

    // Success
    return res.render("auth", {
      type: "FORGOT_PASSWORD",
      success: "New password has been sent to your email address",
    });
  } catch (err) {
    return res.render("auth", {
      type: "FORGOT_PASSWORD",
      error: process.env.ERROR_MESSAGE_500,
    });
  }
};

const logout = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.body._id);
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    await User.updateOne({ where: { _id: user._id } }, { tokens: [""] });
    res.setHeader("Cache-Control", "private");
    res.cookie("jwt_token", "", { sameSite: "lax" });

    return res.redirect("/login");
  } catch (err) {
    return res.render("auth", {
      type: "LOGIN",
      error: process.env.ERROR_MESSAGE_500,
      err,
    });
  }
};

module.exports = {
  register,
  login,
  forgotPassword,
  logout,
};
