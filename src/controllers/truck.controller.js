const User = require("../models/User");
const Truck = require("../models/Truck");

//! DRIVER ONLY
//* GET USER'S TRUCKS
const getTruck = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Retrieve the list of trucks for authorized user
    const filter = { created_by: req.user._id };
    const truck = await Truck.find({ where: filter });
    let message = "";

    // If there are no trucks
    if (truck.length === 0) {
      message = "This user have not created any truck yet";
    }

    // Success
    return res.render("dashboard", {
      type: "LIST",
      user: user,
      list: truck,
      error: message,
    });
  } catch (err) {
    return res.redirect("/user/trucks");
  }
};

//! DRIVER ONLY
//* ADD TRUCK FOR USER
const addTruck = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Create Truck
    const truck = new Truck(req.body);
    truck.created_by = req.user._id;
    truck.type = truck.type.toUpperCase();
    truck.save();

    // Change Driver status
    user.status = "available";
    user.save();

    // Success
    return res.redirect("/user/trucks");
  } catch (err) {
    console.log(err);
    return res.redirect("/user/trucks");
  }
};

//! DRIVER ONLY
//* GET USER'S TRUCK BY ID
const getTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    const truck = await Truck.findOne({
      where: { _id: req.body.id, created_by: req.user._id },
    });
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // If truck was not found
    let message = "";
    if (!truck) {
      message = "Truck not found";
    }

    // Success
    return res.render(`dashboard`, {
      type: "TRUCK",
      user: user,
      item: truck,
      error: message,
    });
  } catch (err) {
    return res.redirect("/user/trucks");
  }
};

//! DRIVER ONLY
//* UPDATE USER'S TRUCK BY ID
const updateTruckById = async (req, res) => {
  try {
    // User authorization
    Truck.findByIdAndUpdate(
      req.body._id,
      { type: req.body.type.toUpperCase() },
      (err, docs) => {
        if (err)
          return res.render("bad_request", {
            error: process.env.ERROR_MESSAGE_401,
          });

        // Success
        if (docs) return res.redirect("/user/trucks");
      }
    );
  } catch (err) {
    return res.redirect("/user/trucks");
  }
};

//! DRIVER ONLY
//* DELETE USER'S TRUCK BY ID
const deleteTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Delete truck
    const filter = { _id: req.body.id };
    await Truck.deleteOne({ where: filter });

    // Success
    return res.redirect("/user/trucks");
  } catch (err) {
    return res.redirect("/user/trucks");
  }
};

//! DRIVER ONLY
//* ASSIGN TRUCK TO USER BY ID
const assignTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    const truck = await Truck.findOne({
      where: { _id: req.body.id, created_by: req.user._id },
    });
    if (!user || !truck)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Assign Truck
    truck.assigned_to = req.user._id;
    truck.save();
    user.status = "available";
    user.save();

    // Success
    return res.redirect("/user/trucks");
  } catch (err) {
    return res.redirect("/user/trucks");
  }
};

module.exports = {
  getTruck,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
