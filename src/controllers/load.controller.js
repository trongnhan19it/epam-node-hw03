const User = require("../models/User");
const Truck = require("../models/Truck");
const Load = require("../models/Load");

//* GET USER'S LOADS
const getLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Retrieve the list of loads for authorized user, returns list of completed and active loads for Driver and list of all available loads for Shipper
    const filter = { created_by: req.user._id };
    const load = await Load.find({ where: filter });

    // If there is no load
    let message = "";
    if (load.length === 0) {
      message = "This user doesn't have any loads";
    }

    // Success
    return res.render("dashboard", {
      type: "LIST",
      user: user,
      list: load,
      error: message,
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! SHIPPER ONLY
//* ADD LOAD FOR USER
const addLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Create Load
    const load = new Load(req.body);
    load.created_by = req.user._id;
    load.save();

    // Success
    return res.redirect("/user/loads");
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! DRIVER ONLY
//* GET USER'S ACTIVE LOAD
const getActiveLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Retrieve the active load for authorized driver
    const filter = { state: /En route to/, assigned_to: req.user._id };
    const load = await Load.find({ where: { filter } });

    // If there is no load
    let message = "";
    if (load.length === 0) {
      message = "No active loads found";
    }

    // Success
    return res.render("dashboard", {
      type: "LOAD",
      user: user,
      list: load,
      error: message,
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! DRIVER ONLY
//* ITERATE TO NEXT LOAD STATE
const setActiveLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Find load
    let filter = { assigned_to: req.user._id };
    let load = await Load.findOne({ where: filter });
    let message = "";
    if (load) {
      // If the load state is completed
      if (load.state === "Completed") {
        message = "The load was already delivered successfully";
      } else {
        // Change load state
        load.state =
          load.state === "En route to Pick Up"
            ? "En route to Delivery"
            : "Completed";
        message = `Load state changed to ${load.state}`;
        load.logs = load.logs.concat({ message: message, time: new Date() });
        load.save();
      }
    } else {
      // If there is no active load
      message = "Active load list is empty";
    }

    // Success
    return res.render("dashboard", {
      type: "LOAD",
      user: user,
      list: [load],
      message: message,
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//* GET USER'S LOAD BY ID
const getLoadById = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    const load = await Load.findOne({
      where: {
        _id: req.body.id,
        $or: [{ created_by: req.user._id }, { assigned_to: req.user._id }],
      },
    });
    if (!user || !load)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Success
    return res.render(`dashboard`, {
      type: "LOAD",
      user: user,
      list: [load],
      success: "Load found",
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! SHIPPER ONLY
//* UPDATE USER'S LOAD BY ID
const updateLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: { _id: req.body.id, created_by: req.user._id },
    });
    if (!user || !load)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Update load
    const filter = { _id: req.body.id, created_by: req.user._id };
    const update = {
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: req.body.dimensions,
    };
    await Load.updateOne({ where: filter, update });

    // Success
    return res.render(`dashboard`, {
      type: "LOAD",
      user: user,
      list: [load],
      success: "Load details changed successfully",
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! SHIPPER ONLY
//* DELETE USER'S LOAD BY ID
const deleteLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Delete load
    await Load.deleteOne({ where: { _id: req.body.id } });

    // Success
    return res.redirect("/user/loads");
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! SHIPPER ONLY
//* POST A USER'S LOAD BY ID
const postLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: {
        _id: req.body.id,
        created_by: req.user._id,
        assigned_to: "n/a",
      },
    });
    if (!user || !load)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_401,
      });

    // Search for one available driver
    const filter = { role: "DRIVER", status: "available" };
    const driver = await User.findOne({ where: filter });

    // If there is no driver
    if (!driver) {
      return res.render("dashboard", {
        type: "LIST",
        user: user,
        error: "No available driver found",
      });
    }

    // Post a user's load by id
    load.state = "En route to Pick Up";
    load.assigned_to = req.user._id;
    load.logs = load.logs.concat({
      message: `Load assigned to driver with id ${req.user._id}`,
    });
    load.save();

    // Success
    return res.redirect("/user/loads/posted");
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

//! SHIPPER ONLY
//* GET USER'S LOAD SHIPPING INFO BY ID
const getLoadShippingInfo = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: {
        _id: req.body.id,
        assigned_to: /^n\/a/,
      },
    });
    const truck = await Truck.findOne({
      where: { assigned_to: load.assigned_to },
    });
    if (!user)
      return res.render("bad_request", {
        error: process.env.ERROR_MESSAGE_400,
      });

    // If there is no shipping info
    let message = "";
    if (!load || !truck) {
      message = "Shipping info not found";
    }

    // Success
    return res.render(`dashboard`, {
      type: "SHIPPING_INFO",
      user: user,
      load: load,
      truck: truck,
      error: message,
    });
  } catch (err) {
    return res.redirect("/user/loads");
  }
};

module.exports = {
  getLoad,
  addLoad,
  getActiveLoad,
  setActiveLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfo,
};
