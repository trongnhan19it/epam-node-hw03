const bcrypt = require("bcryptjs");
const User = require("../models/User");
const Truck = require("../models/Truck");
const Load = require("../models/Load");

//* GET USER'S PROFILE INFO
//! USER CAN REQUEST ONLY HIS OWN PROFILE INFO
const getUser = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    const message = {
      _id: user._id,
      role: user.role,
      email: user.email,
      created_date: user.created_date.toJSON(),
    };
    console.log("User found");
    return res.status(200).send({ user: message });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* DELETE USER'S PROFILE
//! USER CAN DELETE ONLY HIS OWN PROFILE INFO
const deleteUser = async (req, res) => {
  try {
    // User authorization
    User.findByIdAndDelete(req.user._id, async (err) => {
      if (err) {
        console.log("User not found");
        console.log(err);
        return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
      }

      // Delete user info
      const filter = { created_by: req.user._id };
      if (req.user.role === "DRIVER") await Truck.deleteMany({ where: filter });
      else await Load.deleteMany({ where: filter });

      // Success
      const message = "Profile deleted successfully";
      console.log(message);
      return res.status(200).send({ message: message });
    });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* CHANGE USER'S PASSWORD
const changePassword = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Check password
    const passwordIsIncorrect = !(await bcrypt.compare(
      req.body.oldPassword,
      req.user.password
    ));
    if (passwordIsIncorrect) {
      console.log("Passwords is incorrect");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Change password
    const update = await bcrypt.hash(req.body.newPassword, 8);
    await User.updateOne({ where: { _id: user._id } }, { password: update });

    // Token
    const token = await user.generateAuthToken();
    res.setHeader("Cache-Control", "private");
    res.cookie("jwt_token", token, { sameSite: "lax" });

    // Success
    const message = "Password changed successfully";
    console.log(message);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

module.exports = { getUser, deleteUser, changePassword };
