const User = require("../models/User");

//* REGISTER A NEW USER
const register = async (req, res) => {
  try {
    // Get the user credentials
    const newUser = new User(req.body);
    await newUser.save();
    const token = await newUser.generateAuthToken();

    // Success
    const message = "Profile created successfully";
    console.log(message, token);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

//* LOGIN
const login = async (req, res) => {
  try {
    // User authorization
    const { email, password } = req.body;
    console.log(req.body);
    const user = await User.findByCredentials(email, password);
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Generate new login token
    const token = await user.generateAuthToken();
    res.setHeader("Cache-Control", "private");
    res.cookie("jwt_token", token, { sameSite: "lax" });

    // Success
    console.log(`New login token\n> ${token}`);
    return res.status(200).send({ jwt_token: token });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* RESET PASSWORD
const forgotPassword = async (req, res) => {
  try {
    // User authorization
    const user = await User.findOne(req.body);
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    const message = "New password sent to your email address";
    console.log(message);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

module.exports = { register, login, forgotPassword };
