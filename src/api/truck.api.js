const User = require("../models/User");
const Truck = require("../models/Truck");

//* GET USER'S TRUCKS
//! DRIVER ONLY
const getTruck = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Retrieve the list of trucks for authorized user
    const filter = { _id: req.params.id, created_by: req.user._id };
    const truck = await Truck.find({ where: filter });
    if (truck.length === 0) {
      console.log("Truck list is empty");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    console.log("Truck list found");
    return res.status(200).send({ trucks: truck });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* ADD TRUCK FOR USER
//! DRIVER ONLY
const addTruck = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Create Truck
    const truck = new Truck(req.body);
    truck.created_by = req.user._id;
    truck.save();

    // Change Driver status
    user.status = "available";
    user.save();

    // Success
    const message = "Truck created successfully";
    console.log(message);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

//* GET USER'S TRUCK BY ID
//! DRIVER ONLY
const getTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    const truck = await Truck.findOne({
      where: { _id: req.params.id, created_by: req.user._id },
    });
    if (!user || !truck) {
      console.log("User or Truck not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    console.log("Truck found");
    return res.status(200).send({ truck: truck });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* UPDATE USER'S TRUCK BY ID
//! DRIVER ONLY
const updateTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    const truck = await Truck.findOne({
      where: { _id: req.params.id, created_by: req.user._id },
    });
    if (!user || !truck) {
      console.log("User or Truck not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Update truck
    const filter = { _id: req.params.id, created_by: req.user._id };
    const update = { type: req.body.type };
    await Truck.updateOne({ where: filter, update });

    // Success
    console.log("Truck details changed successfully");
    return res
      .status(200)
      .send({ message: "Truck details changed successfully" });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* DELETE USER'S TRUCK BY ID
//! DRIVER ONLY
const deleteTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Delete truck
    const filter = { _id: req.params.id };
    await Truck.deleteOne({ where: filter });

    // Success
    console.log(`Truck with ID: ${req.params.id} deleted successfully`);
    return res.status(200).send({ message: "Truck deleted successfully" });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* ASSIGN TRUCK TO USER BY ID
//! DRIVER ONLY
const assignTruckById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    const truck = await Truck.findOne({
      where: { _id: req.params.id, created_by: req.user._id },
    });
    if (!user || !truck) {
      console.log("User or Truck not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Assign Truck
    truck.assigned_to = req.user._id;
    truck.save();
    user.status = "available";
    user.save();

    // Success
    console.log(`Truck assigned successfully to user with ID: ${req.user._id}`);
    return res.status(200).send({ message: "Truck assigned successfully" });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

module.exports = {
  getTruck,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
