const User = require("../models/User");
const Truck = require("../models/Truck");
const Load = require("../models/Load");

//* GET USER'S LOADS
const getLoad = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Retrieve the list of loads for authorized user, returns list of completed and active loads for Driver and list of all available loads for Shipper
    const filter =
      req.user.role === "DRIVER"
        ? {
            _id: req.params.id,
            status: req.params.status,
            created_by: req.user._id,
          }
        : {
            _id: req.params.id,
            created_by: req.user._id,
          };
    const load = await Load.find({ where: filter });
    if (load.length === 0) {
      console.log("Load list is empty");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    const offset = req.offset || 0;
    const limit = req.limit || 1;
    console.log(`Load list found with offset: ${offset}, limit: ${limit}`);
    return res.status(200).send({ loads: load.slice(offset, offset + limit) });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* ADD LOAD FOR USER
//! SHIPPER ONLY
const addLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Create Load
    const load = new Load(req.body);
    load.created_by = req.user._id;
    load.save();

    // Success
    const message = "Load created successfully";
    console.log(message);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

//* GET USER'S ACTIVE LOAD
//! DRIVER ONLY
const getActiveLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Retrieve the active load for authorized driver
    const filter = { state: /En route to Pick Up/, assigned_to: req.user._id };
    const load = await Load.find({ where: { filter } });
    if (!load) {
      console.log("Active load list is empty");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    console.log(`Active load with ID: ${load._id} found`);
    return res.status(200).send({ load: load });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* ITERATE TO NEXT LOAD STATE
//! DRIVER ONLY
const setActiveLoad = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "DRIVER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Find load
    let filter = { assigned_to: req.user._id };
    let load = await Load.findOne({ where: filter });
    if (load) {
      // If the load state is completed
      if (load.state === "Completed") {
        console.log("The load was already delivered successfully");
        return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
      }

      // Change load state
      load.state =
        load.state === "En route to Pick Up"
          ? "En route to Delivery"
          : "Completed";
      const message = `Load state changed to ${load.state}`;
      load.logs = load.logs.concat({ message: message, time: new Date() });
      load.save();

      // Success
      console.log(message);
      return res.status(200).send({ message: message });
    }

    // If there is no active load
    console.log("Active load list is empty");
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* GET USER'S LOAD BY ID
const getLoadById = async (req, res) => {
  try {
    // User authorization
    const user = await User.findById(req.user._id);
    const load = await Load.findOne({
      where: {
        _id: req.params.id,
        $or: [{ created_by: req.user._id }, { assigned_to: req.user._id }],
      },
    });
    if (!user || !load) {
      console.log("User or Load not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    console.log("Load found");
    return res.status(200).send({ load: load });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* UPDATE USER'S LOAD BY ID
//! SHIPPER ONLY
const updateLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: { _id: req.params.id, created_by: req.user._id },
    });
    if (!user || !load) {
      console.log("User or Truck not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Update load
    const filter = { _id: req.params.id, created_by: req.user._id };
    const update = {
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: req.body.dimensions,
    };
    await Load.updateOne({ where: filter, update });

    // Success
    const message = "Load details changed successfully";
    console.log(message);
    return res.status(200).send({ message: message });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* DELETE USER'S LOAD BY ID
//! SHIPPER ONLY
const deleteLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    if (!user) {
      console.log("User not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Delete load
    await Load.deleteOne({ where: { _id: req.params.id } });

    // Success
    console.log(`Load with ID: ${req.params.id} deleted successfully`);
    return res.status(200).send({ message: "Load deleted successfully" });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: process.env.ERROR_MESSAGE_500 });
  }
};

//* POST A USER'S LOAD BY ID
//! SHIPPER ONLY
const postLoadById = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: {
        _id: req.params.id,
        created_by: req.user._id,
        assigned_to: "n/a",
      },
    });
    if (!user || !load) {
      console.log("User or Truck not found");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Search for one available driver
    const filter = { role: "DRIVER", status: "available" };
    const driver = await User.findOne({ where: filter });
    if (!driver) {
      console.log("No available drivers found");
      return res
        .status(400)
        .send({ message: process.env.ERROR_MESSAGE_400, driver_found: false });
    }

    // Post a user's load by id
    const message = `Load assigned to driver with id ${req.user._id}`;
    load.state = "En route to Pick Up";
    load.assigned_to = req.user._id;
    load.logs = load.logs.concat({ message: message });
    load.save();

    // Success
    console.log(message);
    return res
      .status(200)
      .send({ message: "Load posted successfully", driver_found: true });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

//* GET USER'S LOAD SHIPPING INFO BY ID
//! SHIPPER ONLY
const getShippingInfo = async (req, res) => {
  try {
    // User authorization
    const user =
      req.user.role === "SHIPPER" ? await User.findById(req.user._id) : false;
    const load = await Load.findOne({
      where: {
        _id: req.params.id,
        assigned_to: /^n\/a/,
      },
    });
    const truck = await Truck.findOne({
      where: { assigned_to: load.assigned_to },
    });
    if (!user || !load || !truck) {
      console.log("There was an error while searching for information");
      return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
    }

    // Success
    console.log("Shipping info found");
    return res.status(200).send({ load: load, truck: load });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ message: process.env.ERROR_MESSAGE_400 });
  }
};

module.exports = {
  getLoad,
  addLoad,
  getActiveLoad,
  setActiveLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShippingInfo,
};
